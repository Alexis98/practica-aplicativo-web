//Libreria que conecta con MongoDB
const mongoose = require('mongoose')

//Datos que enviara a la base de datos
const UsuarioSchema = new  mongoose.Schema({
	nombre: String,
	telefono: String,
	mail: String,
	creadoEn: {
		type: Date,
		default: new Date()
	}
})

//Exportacion de la constante UsuarioSchema que toma los datos y los envia a MongoDB
const Usuario = mongoose.model('users', UsuarioSchema)
module.exports = Usuario