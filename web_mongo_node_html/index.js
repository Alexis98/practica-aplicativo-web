//Llamado a la ruta
const path = require('path')

//Llamado a express
const express = require('express')

//Llamado a express-edge
const { config, engine} = require('express-edge')

//Llamado a los datos del cliente para ser almacenados en MongoDB
const Usuario = require('./models/Usuario')

//Llamado a libreria que realiza el puente entre la app y MongoDB
const mongoose = require('mongoose')

//Libreria que permite acceder a los datos dentro de un array
const bodyParser = require('body-parser')

//Constante asignada para poder renderizar pantallas (puede cambiar)
const app = new express()

//Conexion con la base de datos
mongoose.connect('mongodb://localhost/practica')

//Dirigiendo a las vistas
app.use(engine);
app.set('views', `${__dirname}/views`);

//Recibiendo cada uno de los datos dentro de un array recibido del servidor
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))

//Llamado al index
app.get('/', async (req, res) => {
	//res.sendFile(path.resolve(__dirname, 'html/index.html'))
	const usuarios = await Usuario.find({})
	console.log(usuarios)

	res.render('index', {
		usuarios
	})
})

//Llamado a la vista de los detalles
app.get('/usuario/:id', async (req, res) => {
	const usuario = await Usuario.findById(req.params.id)
	console.log(usuario)

	res.render('usuario', {
		usuario
	})
})

//Renderizacion de la vista del registro segun cada id
app.get('/ingresar', (req, res) => {
	//res.sendFile(path.resolve(__dirname, 'html/index.html'))
	res.render('ingresar')
})

//Almacenamiento del dato enviado a la base de datos
app.post('/ingresar/guardar', (req, res) => {
	Usuario.create(req.body, (error, Usuario) => {
		res.redirect('/')
	})
})

//Renderizacion de la vista de edicion segun cada id
app.get('/editar/:id', async (req, res) => {
	//res.sendFile(path.resolve(__dirname, 'html/index.html'))
	
	const usuario = await Usuario.findById(req.params.id)

	console.log(usuario)

	res.render('editar', {
		usuario
	})
})

//Almacenamiento de datos enviados a la base datos
app.post('/editar/guardar', (req, res) => {
	const idUsuario = req.body.id
	console.log(idUsuario)

	Usuario.findByIdAndUpdate(idUsuario, {
		nombre: req.body.nombre,
		telefono: req.body.telefono,
		mail: req.body.mail
	}, (error, usuario) => {
		console.log(error, idUsuario)
		res.redirect('/')
	})

})

//Renderizacion de la vista de eliminacion de usuarios segun cada Id
app.get('/borrar/:id', async (req,res) => {

	const usuario = await Usuario.findById(req.params.id)

	console.log(usuario)

	res.render('borrar', {
		usuario
	})
})

//Almacenamiento de datos que seran enviados al servidor para ser eliminados
app.post('/borrar', (req, res) => {
	const idUsuario = req.body.id
	console.log(idUsuario)

	Usuario.findByIdAndRemove(idUsuario, function(err){
		if(err){
			res.send(err);
		}else{
			res.redirect("/")
		}
	})
})


//Puerto donde el servidor iniciara en el entorno local
app.listen(4000, () => {
	console.log('Aplicación corriendo en el puerto 4000')
})